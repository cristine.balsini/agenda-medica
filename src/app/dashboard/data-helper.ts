export interface PeriodicElement {
  name: string;
  position: number;
  phisician: string;
  speciality: string;
}

export const INFO_DATA: PeriodicElement[] = [
  {
    position: 1,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Jusara Fardo',
    speciality: 'Cardiologia',
  },
  {
    position: 2,
    name: 'Cristine Ferreira Balsini',
    phisician: 'Augusto dos Santos de Oliveira',
    speciality: 'Cardiologia',
  },
  {
    position: 3,
    name: 'Sérgio de Mello',
    phisician: 'João Carlos Martins',
    speciality: 'Cardiologia',
  },
  {
    position: 4,
    name: 'Bruno Antonio Moraes',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
  {
    position: 5,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
  {
    position: 6,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
  {
    position: 7,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
  {
    position: 8,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
  {
    position: 9,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
  {
    position: 10,
    name: 'Maria da Paz de Oliveira',
    phisician: 'Sonia Antunes Vieira',
    speciality: 'Cardiologia',
  },
];
