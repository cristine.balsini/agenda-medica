import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { INFO_DATA } from './data-helper';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent {
  constructor(private router: Router) {}
  logout() {
    this.router.navigateByUrl('/');
  }
  calendar() {
    this.router.navigateByUrl('/calendar');
  }

  displayedColumns: string[] = ['position', 'name', 'phisician', 'speciality'];
  dataSource = INFO_DATA;
  myColors = [
    {
      backgroundColor: '#673ab7',
    },
    {
      backgroundColor: 'rgba(103, 58, 183, .1)',
    },
  ];
  myColors2 = [
    {
      backgroundColor: '#de5721',
    },
    {
      backgroundColor: 'rgba(222, 87, 33, .1)',
    },
  ];
  myColors3 = [
    {
      backgroundColor: '#673ab7',
    },
    {
      backgroundColor: '#de5721',
    },
  ];
  chartOptions = {
    responsive: true,
  };
  chartData = [
    { data: [330], label: 'Atendimentos do dia' },
    { data: [250], label: 'Período anterior' },
  ];
  chartData2 = [
    { data: [30], label: 'Atendimentos cancelados' },
    { data: [200], label: 'Período anterior' },
  ];
  chartData3 = [
    { data: [300], label: 'Atendimentos do dia' },
    { data: [500], label: 'Atendimentos cancelados' },
  ];
}
