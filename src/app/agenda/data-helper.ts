import { EventInput } from '@fullcalendar/angular';

let eventGuid = 0;
const TODAY = new Date().toISOString().replace(/T.*$/, '');

export const INITIAL_EVENTS: EventInput[] = [
  {
    id: createEventId(),
    title: 'João da Silva',
    start: TODAY,
  },
];

export function createEventId() {
  return String(eventGuid++);
}
